## OpenWebRadioPlayer [EN]

This is a simple open-source WebRadioPlayer that needs no backend
services. [Use it via Codeberg Pages!](https://wh0ami.codeberg.page/OpenWebRadioPlayer/public/)

The streaming URLs are stored in `data/data.json`. Feel free to commit more or report broken streams!

The website language is selected by the clients browser language. The default language is english.

The project is using the [Boombox Favicon](https://icons8.com/icons/set/boombox) from [Icons8](https://icons8.com/).
Thank you for that!

#### Hotkeys

| Key           | Function                     |
|---------------|------------------------------|
| -             | Decrease volume              |
| +             | Increase volume              |
| \<space bar\> | mute/unmute stream           |
| s             | stop stream                  |
| f             | focus on search bar          |
| x             | clear search bar             |

---

## OpenWebRadioPlayer [DE]

Dies ist ein simpler, quelloffener WebRadioPlayer, welcher ohne irgendwelche Backend-Dienste
auskommt. [Nutze den Player via Codeberg Pages!](https://wh0ami.codeberg.page/OpenWebRadioPlayer/public/)

Die URLs für das Streaming sind in `data/data.json` gespeichert. Du kannst gerne zusätzliche Streams hinzufügen oder
nicht funktionierende Streams zu melden.

Die Sprache der Website wird basierend auf der Sprache des Browsers ausgewählt. Die Standardsprache ist Englisch.

Das Projekt benutzt das [Boombox Favicon](https://icons8.com/icons/set/boombox) von [Icons8](https://icons8.com/).
Vielen Dank dafür!

#### Hotkeys

| Taste         | Funktion                                     |
|---------------|----------------------------------------------|
| -             | Lautstärke erhöhen                           |
| +             | Lautstärke verringern                        |
| \<Leertaste\> | Stream stummschalten/Stummschaltung aufheben |
| s             | Stream stoppen                               |
| f             | Fokus auf Suchleiste setzen                  |
| x             | Suchleiste leeren                            |

---

## Screenshots

![screenshots/OpenWebRadioPlayer_DE_1.png](screenshots/OpenWebRadioPlayer_DE_1.png)  
![screenshots/OpenWebRadioPlayer_DE_2.png](screenshots/OpenWebRadioPlayer_DE_2.png)  
![screenshots/OpenWebRadioPlayer_DE_3.jpg](screenshots/OpenWebRadioPlayer_DE_3.jpg)  
![screenshots/OpenWebRadioPlayer_DE_4.jpg](screenshots/OpenWebRadioPlayer_DE_4.jpg)  
![screenshots/OpenWebRadioPlayer_EN_1.png](screenshots/OpenWebRadioPlayer_EN_1.png)  
![screenshots/OpenWebRadioPlayer_EN_2.png](screenshots/OpenWebRadioPlayer_EN_2.png)  
