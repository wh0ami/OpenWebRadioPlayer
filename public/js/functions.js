/** function for fast getting an element **/
function getElement(id) {
    return document.getElementById(id);
}

/** function for selecting the player element **/
function getPlayer() {
    return getElement('radioplayer');
}

/** function for writing a message to the browser log/console **/
function writeLog(msg) {
    console.log('[OpenWebRadioPlayer] ' + msg);
}

/** function for empty string check **/
function minusIfEmpty(value) {
    if (value === '') {
        return '-';
    } else {
        return String(value);
    }
}

/** function for loading and playing a stream **/
function playStream(player, icon, url) {
    icon.src = "img/stop.png";
    player.pause();
    player.currentTime = 0;
    url = url.split('?')[0] + '?' + new Date().getTime();
    player.src = url;
    try {
        player.load();
        player.play();
    } catch (e) {
        alert("Error playing stream!");
        console.log(e);
    }
    writeLog('Playing now ' + url);
}

/** function for updating the channel information in the upper box **/
function updatePlayerInfo(name, genre, bitrate, country) {
    let channelName = getElement('channelName');
    let siteTitle = getElement('siteTitle');
    if (name === '') {
        channelName.innerText = language.player.choose;
        siteTitle.innerText = 'OpenWebRadioPlayer';
    } else {
        channelName.innerText = language.player.listento + name;
        siteTitle.innerText = name + ' - OpenWebRadioPlayer';
    }

    getElement('channelGenre').innerText = minusIfEmpty(genre);
    getElement('channelBitrate').innerText = minusIfEmpty(bitrate);
    getElement('channelCountry').innerText = minusIfEmpty(country);
}
