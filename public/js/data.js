/** load the language file and insert the strings to the page **/
let userLang = navigator.language.split('-')[0];
if (! ['de', 'en'].includes(userLang)) {
    userLang = 'en';
}
let language;
fetch('lang/' + userLang + '.json')
    .then(response => response.json())
    .then(data => {
        language = data;

        getElement('channelName').innerText = language.player.choose;
        getElement('channelGenreDescription').innerText = language.player.genre + ':';
        getElement('channelBitrateDescription').innerText = language.player.bitrate + ':';
        getElement('channelBitrateUnit').innerText = language.player.bitrateUnit;
        getElement('channelCountryDescription').innerText = language.player.country + ':';
        getElement('searchbar').placeholder = language.search.searchbar;
        getElement('channellistHeadline').innerText = language.list.headline;
        getElement('mixedContentHint').innerText = language.hint.mixedContent;
        getElement('thirdPartyHint').innerText = language.hint.thirdParty;
        getElement('developerMessage').innerHTML = language.other.developerMessage;
    });

/** load the channel (data) file **/
fetch('data/data.json')
    .then(response => response.json())
    .then(data => {
        /** sort channel list by alphabet of the channels name **/
        data.sort(function (a, b) {
            let x = a.name.toLowerCase();
            let y = b.name.toLowerCase();
            if (x < y) {
                return -1;
            }
            if (x > y) {
                return 1;
            }
            return 0;
        });

        /** store channels globally for getChannels function **/
        window.channelJSON = data;

        /** initial loading if all channels **/
        getChannels('');
    });


/** function for loading the channels **/
function getChannels(searchstring) {
    let json = window.channelJSON;

    let list = getElement('channellist');
    list.innerText = '';

    let message = getElement('channellistMessage');
    message.innerText = '';

    let channel = '';
    let print;


    let data;
    for (let channelID in json) {
        data = json[channelID];

        print = data.name.toLowerCase().includes(searchstring.toLowerCase());

        if (searchstring === '' || print) {
            displayname = data.name
            if (!data.url.startsWith('https://')) {
                displayname += ' [!]'
            }

            channel = '<li><span class="stream" id="channel_' + channelID + '">' + displayname + '</span></li>';
            list.insertAdjacentHTML('beforeend', channel);

            getElement('channel_' + channelID).addEventListener('click', e => {
                let channelData = window.channelJSON[e.target.id.split('_')[1]];

                playStream(getPlayer(), getElement('StopPlayIcon'), channelData.url);
                updatePlayerInfo(channelData.name, channelData.genre, channelData.kbits, channelData.country);
            });
        }
    }

    /** print a message to the channel box, if the channel list is empty **/
    if (channel === '') {
        message.innerText = language.list.empty;
    }
}
