/** play/stop button **/
getElement('StopPlayIcon').addEventListener('click', () => {
    /** get the muteicon and the player as a variable **/
    let muteicon = getElement('StopPlayIcon');
    let player = getPlayer();

    if (muteicon.attributes.src.value === "img/stop.png") {
        player.pause();
        player.src = '';
        muteicon.src = "img/play.png";
        updatePlayerInfo('', '', '', '');
        writeLog('Stopping stream!');
    }
});

/** mute button **/
getElement('muteIcon').addEventListener('click', () => {
    /** get the muteicon and the player as a variable **/
    let muteicon = getElement('muteIcon');
    let player = getPlayer();

    /** if there's the unmuted muteicon, and we click it, we will change to the muted muteicon and mute the player **/
    if (muteicon.attributes.src.value === "img/speaker.png") {
        player.muted = true;
        muteicon.src = "img/mutespeaker.png";
        writeLog('Muted!');
    } else {
        player.muted = false;
        muteicon.src = "img/speaker.png";
        writeLog('Unmuted!');
    }
});

/** volume scale **/
getElement('volumescale').addEventListener('input', () => {
    let display = getElement('volumedisplay');
    let muteicon = getElement('muteIcon');
    let player = getPlayer();

    let volume = getElement('volumescale').value;

    if (volume < 10) {
        display.innerText = ' ' + (volume * 10) + '%';
    } else {
        display.innerText = (volume * 10) + '%';
    }

    player.volume = volume / 10;

    if (volume < 1) {
        muteicon.src = "img/mutespeaker.png";
        writeLog('Muted!');
    } else {
        muteicon.src = "img/speaker.png";
        writeLog("Volume was set to " + volume * 10 + '%');
    }
});

/** reloading and searching in channels if there's input in the searchbar **/
getElement('searchbar').addEventListener('input', e => {
    let searchstring = e.target.value;
    getChannels(searchstring);
});


/** clear the search bar and reload the channel list **/
getElement('clearSearchbar').addEventListener('click', () => {
    let searchbar = getElement('searchbar');
    searchbar.value = '';
    getChannels('');
});

/** hotkeys - will be only triggered, if the user is not focussing the searchbar **/
document.addEventListener('keyup', (e) => {
    if (document.activeElement.id !== 'searchbar') {
        let volumescale = getElement('volumescale');

        switch (e.key) {
            case '-':
                writeLog('Hotkey "-" pressed - decreasing volume');
                volumescale.value--;
                volumescale.dispatchEvent(new Event('input'));
                break;
            case '+':
                writeLog('Hotkey "+" pressed - increasing volume');
                volumescale.value++;
                volumescale.dispatchEvent(new Event('input'));
                break;
            case ' ':
                writeLog('Hotkey " " pressed - clicking mute/unmute button');
                getElement('muteIcon').dispatchEvent(new Event('click'))
                break;
            case 'f':
                writeLog('Hotkey "f" pressed - focussing on searchbar');
                getElement('searchbar').focus();
                break;
            case 'x':
                writeLog('Hotkey "x" pressed - clearing the searchbar');
                getElement('clearSearchbar').dispatchEvent(new Event('click'));
                break;
            case 's':
                writeLog('Hotkey "s" pressed - clicking stop/play icon');
                getElement('StopPlayIcon').dispatchEvent(new Event('click'));
                break;
            default:
                break;
        }
    }
});
